<?php
/**
 * Created by PhpStorm.
 * User: BigFish
 * Date: 16/02/2018
 * Time: 15:27
 */

namespace Wellingtonsm\EloquentServices\Generators;

use Wellingtonsm\EloquentServices\Exception\FileAlreadyExistsException;

class Generator
{
	/**
	 * The array of options.
	 *
	 * @var array
	 */
	protected $options;

	/**
	 * The shortname of stub.
	 *
	 * @var string
	 */
	protected $stub;

	public function __construct(array $options = [])
	{
		$this->options = $options;
	}

	/**
	 * Get template replacements.
	 *
	 * @return array
	 */
	public function getReplacements()
	{
		return [
			'class' => $this->getClass(),
			'namespace' => $this->getNamespace(),
			'root_namespace' => $this->getRootNamespace()
		];
	}

	/**
	 * Get name input.
	 *
	 * @return string
	 */
	public function getName()
	{
		$name = $this->options['name'];

		if (str_contains($name, '\\')) {
			$name = str_replace('\\', '/', $name);
		}
		if (str_contains($name, '/')) {
			$name = str_replace('/', '/', $name);
		}

		return studly_case(str_replace(' ', '/', ucwords(str_replace('/', ' ', $name))));
	}

	/**
	 * Get base path of destination file.
	 *
	 * @return string
	 */
	public function getBasePath()
	{
		return base_path();
	}

	/**
	 * Get class name.
	 *
	 * @return string
	 */
	public function getClass()
	{
		return studly_case(class_basename($this->getName()));
	}

	/**
	 * Get options.
	 *
	 * @return array
	 */
	public function getOptions()
	{
		return $this->options;
	}

	/**
	 * Get application namespace
	 *
	 * @return string
	 */
	public function getAppNamespace()
	{
		return app()->getNamespace();
	}

	/**
	 * Get root namespace.
	 *
	 * @return string
	 */
	public function getRootNamespace()
	{
		return config('eloquent_services.generator.rootNamespace', $this->getAppNamespace());
	}

	/**
	 * Get paths of namespace.
	 *
	 * @return array
	 */
	public function getSegments()
	{
		return explode('/', $this->getName());
	}

	/**
	 * Get class namespace.
	 *
	 * @return string
	 */
	public function getNamespace()
	{
		$segments = $this->getSegments();

		array_pop($segments);

		$rootNamespace = $this->getRootNamespace();

		if ($rootNamespace == false) {
			return null;
		}

		$namespace = rtrim($rootNamespace . '\\' . implode($segments, '\\'), '\\');

		return $namespace;
	}

	/**
	 * Get file name
	 *
	 * @return string
	 */
	public function getFileName()
	{
		return $this->options['name'];
	}

	/**
	 * Get destination path for generated file.
	 *
	 * @return string
	 */
	public function getPath()
	{
		return $this->getBasePath() . $this->getFileName() . '.php';
	}

	/**
	 * Get stub template for generated file.
	 *
	 * @return string
	 */
	public function getStub()
	{
		$path = config('eloquent_services.generator.stubsOverridePath', __DIR__ . '/../stubs/');

		if (!file_exists($path . '/stubs/' . $this->stub . '.stub')) {
			$path = __DIR__ . '/../stubs/';
		}

		return $this->makeStub($path);
	}

	/**
	 * Get stub file and replace content.
	 *
	 * @param  string $path
	 *
	 * @return string
	 */
	public function makeStub($path)
	{
		$content = file_get_contents($path . $this->stub . '.stub');

		foreach ($this->getReplacements() as $search => $replacement) {

			$content = str_replace('__' . strtoupper(snake_case($search)) . '__', $replacement, $content);
		}

		return $content;
	}

	/**
	 * Check if file allow to create.
	 *
	 * @return bool
	 */
	public function allowToCreate()
	{
		if (file_exists($this->getPath())) {
			if (!$this->options['force']) return false;
			return true;
		}

		return true;
	}

	/**
	 * Get class-specific output paths.
	 *
	 * @param $class
	 *
	 * @return string
	 */
	public function getConfigGeneratorClassPath($class, $directoryPath = false)
	{
		switch ($class) {
			case ('models' === $class):
				$path = config('eloquent_services.generator.paths.models', 'Entities');
				break;
			case ('controllers' === $class):
				$path = config('eloquent_services.generator.paths.controllers', 'Http\Controllers');
				break;
            case ('services' === $class):
                $path = config('eloquent_services.generator.paths.services', 'Services');
                break;
            case ('requests' === $class):
                $path = config('eloquent_services.generator.paths.requests', 'Http\Requests');
                break;
			default:
				$path = '';
		}

		if ($directoryPath) {
			$path = str_replace('\\', '/', $path);
		} else {
			$path = str_replace('/', '\\', $path);
		}

		return $path;
	}

	/**
	 * Run the generator.
	 *
	 * @return array
	 * @throws FileAlreadyExistsException
	 */
	public function run()
	{
		$path = $this->getPath();

		if (!$this->allowToCreate()) {
			return [
				'success' => false,
				'message' => title_case($this->stub) . ' ' . $this->getFileName() . ' already exists and option --force is not present'
			];
		}

		if (!is_dir($dir = dirname($path))) {
			mkdir($dir, 0777, true);
		}

		if (file_put_contents($path, $this->getStub()) !== false) {
			return [
				'success' => true
			];
		}
	}
}