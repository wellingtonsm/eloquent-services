<?php
/**
 * Created by PhpStorm.
 * User: BigFish
 * Date: 16/02/2018
 * Time: 15:33
 */

namespace Wellingtonsm\EloquentServices\Generators;

class ModelGenerator extends Generator
{
	/**
	 * Get stub name.
	 *
	 * @var string
	 */
	protected $stub = 'model';

	/**
	 * Get root namespace.
	 *
	 * @return string
	 */
	public function getRootNamespace()
	{
		return str_replace('/', '\\', parent::getRootNamespace() . parent::getConfigGeneratorClassPath($this->getPathConfigNode()));
	}

	/**
	 * Get class namespace.
	 *
	 * @return string
	 */
	public function getNamespace()
	{
		return $this->getRootNamespace();
	}

	/**
	 * Get generator path config node.
	 *
	 * @return string
	 */
	public function getPathConfigNode()
	{
		return 'models';
	}

	/**
	 * Get base path of destination file.
	 *
	 * @return string
	 */
	public function getBasePath()
	{
		return config('eloquent_services.generator.basePath', app_path());
	}

	/**
	 * Get file name
	 *
	 * @return string
	 */
	public function getFileName()
	{
		return studly_case(last($this->getSegments()));
	}

	/**
	 * Get destination path for generated file.
	 *
	 * @return string
	 */
	public function getPath()
	{
		return $this->getBasePath() . '/' . parent::getConfigGeneratorClassPath($this->getPathConfigNode()) . '/' . $this->getFileName(). '.php';
	}

	/**
	 * Get template replacements.
	 *
	 * @return array
	 */
	public function getReplacements()
	{
		return [
			'class' => $this->getClass(),
			'namespace' => $this->getNamespace()
		];
	}
}