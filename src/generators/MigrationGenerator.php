<?php
/**
 * Created by PhpStorm.
 * User: BigFish
 * Date: 16/02/2018
 * Time: 15:33
 */

namespace Wellingtonsm\EloquentServices\Generators;

class MigrationGenerator extends Generator
{
	/**
	 * Get stub name.
	 *
	 * @var string
	 */
	protected $stub = 'migration';

	/**
	 * Get base path of destination file.
	 *
	 * @return string
	 */
	public function getBasePath()
	{
		return base_path() . '/database/migrations/';
	}

	/**
	 * Get file name
	 *
	 * @return string
	 */
	public function getFileName()
	{
		return 'create_' . snake_case(str_plural(last($this->getSegments()))) . '_table' ;
	}

	/**
	 * Get destination path for generated file.
	 *
	 * @return string
	 */
	public function getPath()
	{
		return $this->getBasePath() . date('Y_m_d_His_') . $this->getFileName(). '.php';
	}

	/**
	 * Check if file allow to create.
	 *
	 * @return bool
	 */
	public function allowToCreate()
	{
		$filename = $this->getFileName();
		$migrations = scandir(database_path('migrations'));

		foreach($migrations as $migration){
			if(str_contains($migration, $filename)){
				if($this->options['force']){
					unlink($this->getBasePath() . $migration);
					return true;
				}else{
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Get template replacements.
	 *
	 * @return array
	 */
	public function getReplacements()
	{
		return [
			'class' => str_plural($this->getClass()),
			'table' => snake_case(str_plural(last($this->getSegments())))
		];
	}
}