<?php
/**
 * Created by PhpStorm.
 * User: BigFish
 * Date: 16/02/2018
 * Time: 15:33
 */

namespace Wellingtonsm\EloquentServices\Generators;

class ServiceGenerator extends Generator
{
	/**
	 * Get stub name.
	 *
	 * @var string
	 */
	protected $stub = 'service';

	/**
	 * Get root namespace.
	 *
	 * @return string
	 */
	public function getRootNamespace()
	{
		return str_replace('/', '\\', parent::getRootNamespace() . parent::getConfigGeneratorClassPath($this->getPathConfigNode()));
	}

	/**
	 * Get class namespace.
	 *
	 * @return string
	 */
	public function getNamespace()
	{
		return $this->getRootNamespace();
	}

	/**
	 * Get entity class namespace.
	 *
	 * @return string
	 */
	public function getEntityNamespace()
	{
		return str_replace('/', '\\', parent::getRootNamespace() . parent::getConfigGeneratorClassPath('models'));
	}

	/**
	 * Get generator path config node.
	 *
	 * @return string
	 */
	public function getPathConfigNode()
	{
		return 'services';
	}

	/**
	 * Get base path of destination file.
	 *
	 * @return string
	 */
	public function getBasePath()
	{
		return config('eloquent_services.generator.basePath', app_path());
	}

	/**
	 * Get file name
	 *
	 * @return string
	 */
	public function getFileName()
	{
		return studly_case(last($this->getSegments())) . 'Service';
	}

	/**
	 * Get destination path for generated file.
	 *
	 * @return string
	 */
	public function getPath()
	{
		return $this->getBasePath() . '/' . parent::getConfigGeneratorClassPath($this->getPathConfigNode()) . '/' . $this->getFileName(). '.php';
	}

	/**
	 * Get template replacements.
	 *
	 * @return array
	 */
	public function getReplacements()
	{
		return [
			'class' => $this->getClass(),
			'pluralClass' => str_plural($this->getClass()),
			'namespace' => $this->getNamespace(),
			'entityNamespace' => $this->getEntityNamespace(),
			'resource' => strtolower($this->getClass()),
			'pluralResource' => strtolower(str_plural($this->getClass()))
		];
	}
}