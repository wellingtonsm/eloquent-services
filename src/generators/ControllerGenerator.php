<?php
/**
 * Created by PhpStorm.
 * User: BigFish
 * Date: 16/02/2018
 * Time: 15:33
 */

namespace Wellingtonsm\EloquentServices\Generators;

class ControllerGenerator extends Generator
{
	/**
	 * Get stub name.
	 *
	 * @var string
	 */
	protected $stub = 'controller';

	/**
	 * Get root namespace.
	 *
	 * @return string
	 */
	public function getRootNamespace()
	{
		return str_replace('/', '\\', parent::getRootNamespace() . parent::getConfigGeneratorClassPath($this->getPathConfigNode()));
	}

    /**
     * Get service class namespace.
     *
     * @return string
     */
    public function getServiceNamespace()
    {
        return str_replace('/', '\\', parent::getRootNamespace() . parent::getConfigGeneratorClassPath('services'));
    }

    /**
     * Get request class namespace.
     *
     * @return string
     */
    public function getRequestNamespace()
    {
	    $segments = $this->getSegments();

	    array_pop($segments);

	    $rootNamespace = str_replace('/', '\\', parent::getRootNamespace() . parent::getConfigGeneratorClassPath('requests'));

	    if ($rootNamespace == false) {
		    return null;
	    }

	    $namespace = rtrim($rootNamespace . '\\' . implode($segments, '\\'), '\\');

	    return $namespace;
    }

	/**
	 * Get generator path config node.
	 *
	 * @return string
	 */
	public function getPathConfigNode()
	{
		return 'controllers';
	}

	/**
	 * Get base path of destination file.
	 *
	 * @return string
	 */
	public function getBasePath()
	{
		return config('eloquent_services.generator.basePath', app_path());
	}

	/**
	 * Get file name
	 *
	 * @return string
	 */
	public function getFileName()
	{
		return studly_case($this->options['name']) . 'Controller';
	}

	/**
	 * Get destination path for generated file.
	 *
	 * @return string
	 */
	public function getPath()
	{
		return $this->getBasePath() . '/' . parent::getConfigGeneratorClassPath($this->getPathConfigNode()) . '/' . $this->getFileName(). '.php';
	}

	/**
	 * Get template replacements.
	 *
	 * @return array
	 */
	public function getReplacements()
	{
		return [
			'class' => $this->getClass(),
			'pluralClass' => str_plural($this->getClass()),
			'namespace' => $this->getNamespace(),
            'serviceNamespace' => $this->getServiceNamespace(),
            'requestNamespace' => $this->getRequestNamespace(),
			'resource' => strtolower($this->getClass()),
			'pluralResource' => strtolower(str_plural($this->getClass()))
		];
	}
}