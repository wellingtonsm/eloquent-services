<?php

return [

	'generator' => [
		'basePath' => app()->path(),
		'rootNamespace' => 'App\\',
		'stubsOverridePath' => '',
		'paths' => [
			'models' => 'Entities',
			'controllers' => 'Http/Controllers',
            'services' => 'Services',
            'requests' => 'Http/Requests'
		]
	]

];