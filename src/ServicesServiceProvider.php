<?php

namespace Wellingtonsm\EloquentServices;

use Illuminate\Support\ServiceProvider;
use Wellingtonsm\EloquentServices\Commands\MakeService;

class ServicesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
	    $this->publishes([
		    __DIR__ . '/config/eloquent_services.php' => config_path('eloquent_services.php')
	    ]);

	    $this->mergeConfigFrom(__DIR__ . '/config/eloquent_services.php', 'eloquent_services');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->commands(MakeService::class);
    }
}
