<?php

namespace Wellingtonsm\EloquentServices\Commands;

use Wellingtonsm\EloquentServices\Generators\ControllerGenerator;
use Wellingtonsm\EloquentServices\Generators\MigrationGenerator;
use Wellingtonsm\EloquentServices\Generators\ModelGenerator;
use Wellingtonsm\EloquentServices\Generators\RequestGenerator;
use Wellingtonsm\EloquentServices\Generators\ServiceGenerator;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;

class MakeService extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'service:make {name} {--f|force} {--skip-migration} {--skip-controller} {--skip-model}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a Eloquent Service with contains (Entity, Controller, Migration, Service)';

	/**
	 * @var Collection
	 */
	protected $generators = null;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->generators = collect();

        $options = $this->options();
        $arguments = $this->arguments();

	    if(!$options['skip-migration']){
		    $this->generators->push(new MigrationGenerator([
			    'name' => $arguments['name'],
			    'force' => $options['force']
		    ]));
	    }

	    if(!$options['skip-controller']){
		    $this->generators->push(new ControllerGenerator([
			    'name' => $arguments['name'],
			    'force' => $options['force']
		    ]));

		    $this->generators->push(new RequestGenerator([
                'name' => $arguments['name'],
                'force' => $options['force']
            ]));
	    }

	    if(!$options['skip-model']){
		    $this->generators->push(new ModelGenerator([
			    'name' => $arguments['name'],
			    'force' => $options['force']
		    ]));
	    }

	    $this->generators->push(new ServiceGenerator([
		    'name' => $arguments['name'],
		    'force' => $options['force']
	    ]));

        foreach ($this->generators as $generator){
        	$return = $generator->run();

        	if($return['success']){
        		if(array_has($return, 'message')){
        			if($return['message'] != ''){
				        $this->info($return['message']);
			        }
		        }
	        }else{
        		$this->error($return['message']);
	        }
        }

        $this->info('Service ' . last(explode('/', $this->argument('name'))) . ' was successfully created');
    }
}
